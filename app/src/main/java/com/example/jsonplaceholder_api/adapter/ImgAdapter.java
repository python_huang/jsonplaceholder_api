package com.example.jsonplaceholder_api.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.jsonplaceholder_api.ImgLoader;
import com.example.jsonplaceholder_api.network.ImgModel;
import com.example.jsonplaceholder_api.R;

import java.util.Objects;

public class ImgAdapter extends PagedListAdapter<ImgModel, ImgAdapter.ViewHolder> {

    private final LayoutInflater layoutInflater;
    private final ImgLoader imgLoader;
    private final ImgAdapter.ImgClickListener imgClickListener;

    public ImgAdapter(Context context, ImgClickListener imgClickListener) {
        super(diffCallback);
        this.imgClickListener = imgClickListener;
        layoutInflater = LayoutInflater.from(context);
        imgLoader = new ImgLoader(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.layout_img_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            ImgModel imgModel = getItem(position);
            holder.id.setText(Objects.requireNonNull(imgModel).getId() + "");
            holder.title.setText(imgModel.getTitle());
            imgLoader.setImg(imgModel.getThumbnailUrl(), holder.img);

            holder.img.setOnClickListener(view -> imgClickListener.onClicked(imgModel));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final DiffUtil.ItemCallback<ImgModel> diffCallback = new DiffUtil.ItemCallback<ImgModel>() {
        @Override
        public boolean areItemsTheSame(@NonNull ImgModel oldItem, @NonNull ImgModel newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @SuppressLint("DiffUtilEquals")
        @Override
        public boolean areContentsTheSame(@NonNull ImgModel oldItem, @NonNull ImgModel newItem) {
            return oldItem == newItem;
        }
    };

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final ImageView img;
        private final TextView id;
        private final TextView title;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.item_img);
            id = itemView.findViewById(R.id.item_id);
            title = itemView.findViewById(R.id.item_title);
        }
    }

    public interface ImgClickListener {
        void onClicked(ImgModel imgModel);
    }

}
