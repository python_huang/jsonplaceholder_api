package com.example.jsonplaceholder_api.paging;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.example.jsonplaceholder_api.network.ImgModel;

public class ImgRepository extends DataSource.Factory {

    private final ImgDataSource imgDataSource;
    private final LiveData<Integer> loadstate_mutableLiveData;

    @NonNull
    @Override
    public DataSource create() {
        return imgDataSource;
    }

    public ImgRepository() {
        imgDataSource = new ImgDataSource();
        MutableLiveData<PageKeyedDataSource<Long, ImgModel>> imgSet_mutableLiveData = new MutableLiveData<>();
        imgSet_mutableLiveData.postValue(imgDataSource);
        loadstate_mutableLiveData = imgDataSource.getLoadState_mutablelivedata();
    }

    public LiveData<Integer> getLoadstate_mutableLiveData() {
        return loadstate_mutableLiveData;
    }

}
