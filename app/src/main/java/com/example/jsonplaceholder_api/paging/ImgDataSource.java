package com.example.jsonplaceholder_api.paging;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.PageKeyedDataSource;

import com.example.jsonplaceholder_api.network.ApiManager;
import com.example.jsonplaceholder_api.network.ImgModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;

public class ImgDataSource extends PageKeyedDataSource<Long, ImgModel> {

    private final int page_per_size = 400;

    private final MutableLiveData<Integer> loadstate_mutablelivedata = new MutableLiveData<>();

    public MutableLiveData<Integer> getLoadState_mutablelivedata() {
        return loadstate_mutablelivedata;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Long> params,
                            @NonNull final LoadInitialCallback<Long, ImgModel> callback) {
        Log.i("Data", "Call");
        loadstate_mutablelivedata.postValue(1);
        long since = 0;
        ApiManager.getINSTANCE()
                .getApiService()
                .getImgs(String.valueOf(since), String.valueOf(page_per_size))
                .enqueue(new Callback<List<ImgModel>>() {
                    @Override
                    public void onResponse(@NotNull Call<List<ImgModel>> call,
                                           @NotNull retrofit2.Response<List<ImgModel>> response) {
                        try {
                            Log.i("Size", Objects.requireNonNull(response.body()).size() + "");
                            callback.onResult(response.body(), null,
                                    (long) response.body().get(response.body().size() - 1).getId());
                            loadstate_mutablelivedata.postValue(2);
                            return;
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("DataSize", "Error");
                        }
                        loadInitial(params, callback);
                        loadstate_mutablelivedata.postValue(3);
                    }

                    @Override
                    public void onFailure(@NotNull Call<List<ImgModel>> call, @NotNull Throwable t) {
                        loadInitial(params, callback);
                        loadstate_mutablelivedata.postValue(3);
                    }
                });
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Long> params,
                           @NonNull LoadCallback<Long, ImgModel> callback) {
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Long> params,
                          @NonNull final LoadCallback<Long, ImgModel> callback) {
        Log.i("Key", String.valueOf(params.key));
        loadstate_mutablelivedata.postValue(1);
        ApiManager.getINSTANCE()
                .getApiService()
                .getImgs(String.valueOf(params.key), String.valueOf(page_per_size))
                .enqueue(new Callback<List<ImgModel>>() {
                    @Override
                    public void onResponse(@NotNull Call<List<ImgModel>> call,
                                           @NotNull retrofit2.Response<List<ImgModel>> response) {
                        try {
                            if (Objects.requireNonNull(response.body()).size() == 0) {
                                callback.onResult(response.body(), null);
                                loadstate_mutablelivedata.postValue(2);
                                return;
                            }
                            callback.onResult(response.body(),
                                    (long) response.body().get(response.body().size() - 1).getId());
                            loadstate_mutablelivedata.postValue(2);
                            return;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        loadAfter(params, callback);
                        loadstate_mutablelivedata.postValue(3);
                    }

                    @Override
                    public void onFailure(@NotNull Call<List<ImgModel>> call, @NotNull Throwable t) {
                        loadAfter(params, callback);
                        loadstate_mutablelivedata.postValue(3);
                    }
                });
    }

}
