package com.example.jsonplaceholder_api.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.example.jsonplaceholder_api.paging.ImgRepository;

public class MainViewModel extends BaseViewModel {

    private final LiveData imgPagedList;
    private final ImgRepository imgRepository;

    public MainViewModel() {
        imgRepository = new ImgRepository();
        PagedList.Config config = new PagedList.Config
                .Builder()
                .setEnablePlaceholders(false)
                .setPageSize(200)
                .build();
        imgPagedList = new LivePagedListBuilder(imgRepository, config).build();
    }

    public LiveData getImgPagedList() {
        return imgPagedList;
    }

    public LiveData<Integer> getLoadState_mutableLiveData() {
        return imgRepository.getLoadstate_mutableLiveData();
    }

}
