package com.example.jsonplaceholder_api.network;

public class ImgModel {

    private int albumId;
    private int id;
    private String title;
    private String url;
    private String thumbnailUrl;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

}
