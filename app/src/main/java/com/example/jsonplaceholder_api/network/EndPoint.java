package com.example.jsonplaceholder_api.network;

public class EndPoint {

    public EndPoint() {
    }

    public String getBaseUrl() {
        return "https://jsonplaceholder.typicode.com/";
    }

}
