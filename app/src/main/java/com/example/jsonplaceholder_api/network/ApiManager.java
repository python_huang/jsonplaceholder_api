package com.example.jsonplaceholder_api.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiManager {

    private final EndPoint endPoint;
    private static ApiManager INSTANCE = null;
    private Retrofit retrofit;
    private OkHttpClient okHttpClient;
    private final ApiService apiService;

    public synchronized static ApiManager getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new ApiManager();
        }
        return INSTANCE;
    }

    private ApiManager() {
        endPoint = new EndPoint();
        initOkhttp();
        initRetrofit();
        apiService = retrofit.create(ApiService.class);
    }

    private void initOkhttp() {
        okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10,TimeUnit.SECONDS)
                .build();
    }

    public OkHttpClient getOkHttpClient() {
        return okHttpClient;
    }

    private void initRetrofit() {
        retrofit = new Retrofit.Builder()
                .baseUrl(endPoint.getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    public ApiService getApiService(){
        return apiService;
    }

}
