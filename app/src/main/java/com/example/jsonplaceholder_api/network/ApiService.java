package com.example.jsonplaceholder_api.network;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("photos")
    Call<List<ImgModel>> getImgs(@Query("_start") String start,
                                 @Query("_limit") String size);

}
