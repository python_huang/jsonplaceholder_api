package com.example.jsonplaceholder_api.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.jsonplaceholder_api.adapter.ImgAdapter;
import com.example.jsonplaceholder_api.network.ImgModel;
import com.example.jsonplaceholder_api.viewmodel.MainViewModel;
import com.example.jsonplaceholder_api.R;

public class MainActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private ImgAdapter imgAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private MainViewModel mainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);
    }

    @Override
    void setupView() {
        recyclerView = findViewById(R.id.main_recyclerview);
        swipeRefreshLayout = findViewById(R.id.main_swiperefresh);
    }

    @Override
    void init() {
        mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        imgAdapter = new ImgAdapter(MainActivity.this, imgModel -> imgClicked(imgModel));
        Log.i(TAG, "Created adapter");

        recyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        recyclerView.setAdapter(imgAdapter);
        Log.i(TAG, "Set adapter");

        observerData();
    }

    private void observerData() {
        mainViewModel.getImgPagedList().observe(this, new Observer<PagedList<ImgModel>>() {
            @Override
            public void onChanged(PagedList<ImgModel> imgModels) {
                imgAdapter.submitList(imgModels);
            }
        });

        mainViewModel.getLoadState_mutableLiveData().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if (integer != null) {
                    switch (integer) {
                        case 0:
                            swipeRefreshLayout.setRefreshing(false);
                            break;
                        case 1:
                            swipeRefreshLayout.setRefreshing(true);
                            break;
                        case 2:
                            swipeRefreshLayout.setRefreshing(false);
                            break;
                        case 3:
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(MainActivity.this,
                                            "Api timeout. Tring again",
                                            Toast.LENGTH_SHORT)
                                            .show();
                                }
                            });
                            swipeRefreshLayout.setRefreshing(false);
                            break;
                        default:
                            swipeRefreshLayout.setRefreshing(false);
                            break;
                    }
                }
            }
        });

    }

    private void imgClicked(ImgModel imgModel) {
        Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
        intent.putExtra("id", String.valueOf(imgModel.getId()));
        intent.putExtra("title", imgModel.getTitle());
        intent.putExtra("thumbnailUrl", imgModel.getThumbnailUrl());
        startActivity(intent);
    }

}