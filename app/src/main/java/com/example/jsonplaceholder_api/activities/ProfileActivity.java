package com.example.jsonplaceholder_api.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jsonplaceholder_api.ImgLoader;
import com.example.jsonplaceholder_api.R;

public class ProfileActivity extends BaseActivity {

    private Intent intent;
    private ImgLoader imgLoader;

    private ImageView imageView;
    private TextView id, title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_profile);
        super.onCreate(savedInstanceState);
    }

    @Override
    void setupView() {

        intent = getIntent();
        imgLoader = new ImgLoader(ProfileActivity.this);

        imageView = findViewById(R.id.profile_img);
        id = findViewById(R.id.profile_id);
        title = findViewById(R.id.profile_title);
        imageView.setOnClickListener(v -> viewClicked());
    }

    @Override
    void init() {
        imgLoader.setImg(intent.getStringExtra("thumbnailUrl"), imageView);
        id.setText(intent.getStringExtra("id"));
        title.setText(intent.getStringExtra("title"));
    }

    private void viewClicked() {
        finish();
    }

}