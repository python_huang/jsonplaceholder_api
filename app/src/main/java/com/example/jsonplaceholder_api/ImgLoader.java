package com.example.jsonplaceholder_api;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.widget.ImageView;

import com.example.jsonplaceholder_api.network.ApiManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ImgLoader {

    private final MemoryCache memoryCache = new MemoryCache();
    private final ImgCache imgCache;

    private final Map<ImageView, String> imageViews =
            Collections.synchronizedMap(new WeakHashMap<>());

    private final ExecutorService executorService;
    private final Handler handler = new Handler();

    private final int stub_id = R.drawable.ic_launcher_foreground;

    public ImgLoader(Context context) {
        imgCache = new ImgCache(context);
        executorService = Executors.newFixedThreadPool(5);
    }

    public void setImg(String url, ImageView imageView) {
        imageViews.put(imageView, url);
        Bitmap bitmap = memoryCache.get(url);

        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            queueImg(url, imageView);
            imageView.setImageResource(stub_id);
        }

    }

    private void queueImg(String url, ImageView imageView) {
        ImgToLoad imgToLoad = new ImgToLoad(url, imageView);
        executorService.submit(new ImgsLoader(imgToLoad));
    }

    private static class ImgToLoad {
        final String url;
        final ImageView imageView;

        ImgToLoad(String u, ImageView i) {
            url = u;
            imageView = i;
        }
    }

    class ImgsLoader implements Runnable {

        final ImgToLoad imgToLoad;

        ImgsLoader(ImgToLoad imgToLoad) {
            this.imgToLoad = imgToLoad;
        }

        @Override
        public void run() {
            try {
                if (imageViewReused(imgToLoad)) {
                    return;
                }
                Bitmap bitmap = getBitmap(imgToLoad.url);
                memoryCache.put(imgToLoad.url, bitmap);

                if (imageViewReused(imgToLoad)) {
                    return;
                }

                BitmapDisplayer bitmapDisplayer = new BitmapDisplayer(bitmap, imgToLoad);
                handler.post(bitmapDisplayer);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }

        }

    }

    private Bitmap getBitmap(String url) {
        File f = imgCache.getImg(url);
        Bitmap b = decodeImg(f);
        if (b != null) {
            return b;
        }
        Bitmap bitmap;
        FileOutputStream fos;

        try {
            OkHttpClient client = ApiManager.getINSTANCE().getOkHttpClient();
            Request request = new Request.Builder().url(url).build();
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                throw new IOException(response + "");
            }
            fos = new FileOutputStream(f);
            fos.write(Objects.requireNonNull(response.body()).bytes());
            fos.flush();
            fos.close();

            bitmap = decodeImg(f);
            FileOutputStream outputStream = new FileOutputStream(f.getAbsoluteFile());
            Objects.requireNonNull(bitmap).compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            return bitmap;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            if (throwable instanceof OutOfMemoryError) {
                memoryCache.clear();
            }
            return null;
        }

    }

    private Bitmap decodeImg(File f) {

        try {
            BitmapFactory.Options o1 = new BitmapFactory.Options();
            o1.inJustDecodeBounds = true;
            FileInputStream stream1 = new FileInputStream(f);
            BitmapFactory.decodeStream(stream1, null, o1);
            stream1.close();

            final int REQUIRED_SIZE = 85;
            int width_tmp = o1.outWidth;
            int height_tmp = o1.outHeight;
            int scale = 1;
            while (width_tmp / 2 >= REQUIRED_SIZE && height_tmp / 2 >= REQUIRED_SIZE) {
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inJustDecodeBounds = false;
            o2.inSampleSize = scale;
            FileInputStream stream2 = new FileInputStream(f);
            Bitmap bitmap = BitmapFactory.decodeStream(stream2, null, o2);
            stream2.close();
            return bitmap;
        } catch (FileNotFoundException ignored) {
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean imageViewReused(ImgToLoad imgToLoad) {
        String tag = imageViews.get(imgToLoad.imageView);
        return tag == null || !tag.equals(imgToLoad.url);
    }

    class BitmapDisplayer implements Runnable {

        final Bitmap bitmap;
        final ImgToLoad imgToLoad;

        BitmapDisplayer(Bitmap b, ImgToLoad i) {
            bitmap = b;
            imgToLoad = i;
        }

        public void run() {
            if (imageViewReused(imgToLoad)) {
                return;
            }

            if (bitmap != null) {
                imgToLoad.imageView.setImageBitmap(bitmap);
            } else {
                imgToLoad.imageView.setImageResource(stub_id);
            }

        }

    }

}
