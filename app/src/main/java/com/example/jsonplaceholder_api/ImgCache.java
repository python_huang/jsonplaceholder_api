package com.example.jsonplaceholder_api;

import android.content.Context;

import java.io.File;

public class ImgCache {

    private final File cacheDir;

    public ImgCache(Context context) {
        cacheDir = context.getCacheDir();
    }

    public File getImg(String url) {
        String imgName = String.valueOf(url.hashCode());
        return new File(cacheDir, imgName);
    }

    public void clear() {
        File[] files = cacheDir.listFiles();
        if (files == null) {
            return;
        }
        for (File f : files) {
            if (f != null) {
                f.delete();
            }
        }
    }

}
